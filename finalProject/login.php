<?php

session_cache_limiter('none');
session_start();

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

if(isset($_POST['login']))
		{
			$inUsername = $_POST['inUsername'];
			$inPassword = $_POST['inPassword'];	
				
			include 'connection.php';
				
			$sql = "SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = ? AND event_user_password = ?";
				
			$query = $connection->prepare($sql);
			$query->bind_param("ss", $inUsername, $inPassword);
			$query->execute();
			$query->bind_result($usernameBind, $passwordBind);
			$query->store_result();
			$query->fetch();
				
			if($query->num_rows == 1)
				{
					$_SESSION['validUser'] = "yes";
					
				}
			else
				{
					$_SESSION['validUser'] = "no";
					header('Location: index.php');
				}
			$query->close();
			$connection->close();
		}
	if($_SESSION['validUser'] == "yes")
		{
			header('Location: index.php');
		}

?>
