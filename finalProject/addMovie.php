<?php

session_cache_limiter('none');
session_start();

	$movieNameError = "";
	$movieGenreError = "";
	$movieRatingError = "";
	$movieTimeError = "";
	$movieDescriptionError = "";

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
	//
	//LOGIN DROPDOWN
	//
		$navBarOptions = "        
		<li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
			<form method='post' name='loginForm' action='login.php' class='dropdown-menu'>
    
    			<p>Username:</p> 
        		<input type='text' class='blackText' name='inUsername' />
        		<p>Password:</p>
        		<input type='password' class='blackText' name='inPassword' />
        		<p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
			</form>
        </li>";
}

else{
	$navBarOptions = "
	
         
         		
            		<li><a href='shop-page-mens.html'>Add Movie</a></li>
            		<li><a href='logout.php'>Logout</a></li>
          		
   ";
	}

//
//UPDATE MOVIE
//

if($_SESSION['validUser'] == "yes"){

	include 'connection.php';
	
	if(isset($_POST['submit'])){
		
		$movie_name = $_POST['movie_name'];
		$movie_description = $_POST['movie_description'];
		$movie_genre = $_POST['movie_genre'];
		$movie_rating = $_POST['movie_rating'];
		$movie_time = $_POST['movie_time'];
		
		$sql = "INSERT INTO movie_table (";
		$sql .= "movie_name, ";
		$sql .= "movie_description, ";
		$sql .= "movie_genre, ";
		$sql .= "movie_rating, ";
		$sql .= "movie_time ";
		
		$sql .= ") VALUES (";
		$sql .= "'$movie_name', ";
		$sql .= "'$movie_description', ";
		$sql .= "'$movie_genre', ";
		$sql .= "'$movie_rating', ";
		$sql .= "'$movie_time' ";
		$sql .= ")";
		
		$query = $connection->prepare($sql);
		
		$query->bind_param('sssss',$movie_name,$movie_description,$movie_genre,$movie_rating,$movie_time);
		
		if ( $query->execute() ){
			//echo $event_name;
			$message = "<h1 class='text-center'>Your movie has been successfully added.</h1>";
			$message .= "<p class='text-center'>This page will auto-redirect in 5 seconds. If not, click <a href='index.php'>here.</a></p>";
			header( "refresh:5;url=index.php" );
		}
		else{
			$message = "<h1>You have encountered a problem. Please try again.</h1>";
		}
		
		$query->close();
		$connection->close();
		
	}//end post submit
	

}//end valid user
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Movie</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/styles.css" rel="stylesheet" type="text/css">

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">
    
    <?php if($_SESSION['validUser'] == "yes"){ 
	
		if(isset($_POST["submit"]))
			{
				echo $message;
			}
		else{
	
	?>
    
		<form name="form1" method="post" action="addMovie.php" class="text-center">
  			<p>&nbsp;</p>
  		<p>
    		<label>Movie Name:
      			<input type="text" name="movie_name" id="movie_name" ><span class="error"><?php echo $movieNameError; ?></span>
    		</label>
  		</p>
  		<p>
        	<label>Movie Genre: 
   				<input type="text" name="movie_genre" id="movie_genre" ><span class="error"><?php echo $movieGenreError; ?></span>
            </label>
  		</p>
  		<p>
        	<label>Movie Rating: 
    			<input type="text" id="movie_rating" name="movie_rating"><span class="error"><?php echo $movieRatingError; ?></span>
            </label>
  		</p>
  		<p>
        	<label>Movie Time: 
    			<input type="text" name="movie_time" id="movie_time" ><span class="error"><?php echo $movieTimeError; ?></span>
            </label>
  		</p>
 
  		<p>
    		<label> Movie Description:<br>
      			<textarea style="width:100%" name="movie_description" id="movie_description" cols="45" rows="5"></textarea>
    		</label>
    			<span class="error"><?php echo $movieDescriptionError; ?></span>
  		</p>

  				
		<p>
    		<input type="submit" name="submit" id="button" value="Submit">
    		<input type="reset" name="reset" id="button2" value="Reset">
  		</p>
		</form>
        
     <?php }
	 }
	 
	 	else{
	 ?>
     <p class="text-center jumbotron">Please Login</p>
     <?php
		}
		
	 ?>
    </div>
</div>
</body>
</html>